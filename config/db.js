const mongoose = require('mongoose')
const config = require('config')
const colors = require('colors')
const db = config.get('mongoURI')

const connectDB = async () => {
    try {
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })
        console.log('Database Connected...'.rainbow.bold)
    } catch (error) {
        console.error(error.message)
        process.exit(1)
    }
}

module.exports = connectDB