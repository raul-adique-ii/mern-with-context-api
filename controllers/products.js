const colors = require('colors')
const Product = require('../models/Products')

exports.createProduct = async (req, res) => {
    const { category, description, name, price, status  } = req.body
    
    try {
        // See if the product exist
        let product = await Product.findOne({ name })
      
        if (product) {
            return res.status(400).json({ errors: [ { message: 'Product already exists' } ] })
        }

        // Create instance of the product
        product = new Product({
            category,
            description,
            name,
            price,
            status
        })

        // Save product
        await product.save()
        res.send('Post')
    } catch (error) {
        // console.error(error.message)
        // res.status(500).send('Server Error')
        if (error.name === 'ValidationError') {
            const message = Object.values(error.errors).map(val => val.message)

            return res.status(400).json({ error: message })
        } else {
            return res.status(500).json({
                error: 'Server Error'
            })
        }
    }
}

exports.getProducts = async (req, res) => {
    try {
        const products = await Product.find()
        res.json(products)
    } catch (error) {
        console.error(error.message)
        res.status(500).send('Server Error'.red.bold)
    }
}

exports.updateProduct = async (req, res) => {
    // const productId = req.params.productId
    const { category, description, name, price, status } = req.body
    const productField = {}
        if (category) {
            productField.category = category.split(',').map(c => c.trim())
        }
        if (description) productField.description = description
        if (name) productField.name = name
        if (price) productField.price = price
        if (status) productField.status = status
    try {
        let product = await Product.findById(req.params.productId)

        if (!product) {
            const error = new Error('No Products')
            error.statusCode = 404
            throw error
        }
        if (product) {
            product = await Product.findByIdAndUpdate({ _id: req.params.productId }, { $set: productField }, { new: true })
            return res.json(product)
        }
        console.log(product)
    // await product.save()
    res.send('Updated')
    } catch (error) {
        console.error(error.message)
        res.status(500).send('Server Error')
    }
}

exports.deleteProduct = async (req, res) => {
    try {
        await Product.findOneAndRemove({ _id: req.params.productId })
        res.json({ message: 'Product Deleted' })
    } catch (error) {
        console.error(error.message)
        res.status(500).send('Server Error'.red.bold)
    }
}