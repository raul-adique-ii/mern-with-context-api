const express = require('express')
const colors = require('colors')
const cors = require('cors')
const connectDB = require('./config/db')
const productsRoute = require('./routes/products')

const app = express()

connectDB()

app.use(express.json({ extended: false }))


app.use('/api/products', cors(), productsRoute)

const PORT = 5000

app.listen(PORT, () => console.log(`Server running on port ${PORT}`.america.italic))