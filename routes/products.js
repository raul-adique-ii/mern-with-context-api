const express = require('express')
const router = express.Router()

const products = require('../controllers/products')

// CREATE PRODUCT
router.post('/', products.createProduct)
// READ PRODUCT
router.get('/', products.getProducts)
// UPDATE PRODUCT
router.put('/:productId', products.updateProduct)
// DELETE PRODUCT
router.delete('/:productId', products.deleteProduct)

module.exports = router