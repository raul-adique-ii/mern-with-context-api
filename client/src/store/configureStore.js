import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'

import reducer from './reducer'
import api from './middleware/api'

export const store = configureStore({  
        reducer,
        middleware: [ ...getDefaultMiddleware(), api ]  
})