import axios from 'axios'

import * as actions from '../api'

const api = ({ dispatch }) => next => async action => {
    if (action.type !== actions.apiCallBegan.type) {
        return next(action)
    }

    next(action)

    // MAKE API CALL
    const { url, method, data, onSuccess, onError } = action.payload

    try {     
        const response = await axios.request({
            baseURL: 'http://localhost:5000/api',
            url,
            method,
            data
        })
        // GENERAL
        dispatch(actions.apiCallSuccess(response.data))
        // SPECIFIC
        dispatch({ type: onSuccess, payload: response.data })
    } catch (error) {
        // GENERAL
        dispatch(actions.apiCallFailed(error))
        // SPECIFIC
        if (error) dispatch({ type: onError, payload: error })
    }
}

export default api