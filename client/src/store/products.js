import { createSlice } from '@reduxjs/toolkit'
import { apiCallBegan } from './api'

const productSlice = createSlice({
    name: 'products',
    initialState: {
        list: [],
        loading: false,
        lastFetch: null
    },
    reducers: {
        productsReceived: (products, action) => {
            products.list = action.payload
        }
    }
})

export const { productsReceived } = productSlice.actions
export default productSlice.reducer

// ACTION CREATORS
const url = '/products'

export const loadProducts = () => apiCallBegan({
    url,
    onSuccess: productsReceived.type
})