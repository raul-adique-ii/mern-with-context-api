import React from 'react';
import ReactDOM from 'react-dom';
// import { Provider } from 'react-redux'

// import {store} from './store/configureStore'

import { GlobalProvider } from './context/products'

import App from './App';

ReactDOM.render(
  <React.StrictMode>
    {/* <Provider store={store}> */}
    <GlobalProvider>
      <App />
    </GlobalProvider>
    {/* </Provider> */}
  </React.StrictMode>,
  document.getElementById('root')
);