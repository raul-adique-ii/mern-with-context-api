import React from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    root: {
        color: 'lightseagreen',
        fontFamily: 'Sigmar One, cursive',
        fontSize: '40px',
        margin: theme.spacing(3, 0, 2),
        textAlign: 'center',
        textShadow: '2px 2px midnightblue'
    }
}))

const Header = () => {
    const classes = useStyles()
    return (
        <Typography className={classes.root} component='h1'>
            CRUD using mern with context api
        </Typography>
    )
}

export default Header
