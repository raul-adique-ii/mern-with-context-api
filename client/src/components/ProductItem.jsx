import React, { useContext } from 'react'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography';

import { GlobalContext } from '../context/products'

const useStyles = makeStyles({
    root: {
      minWidth: 275,
      backgroundColor: '#263238',
      color: '#f4f4f4'
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  });

const ProductItem = ({ product }) => {
    const classes = useStyles();
    const { deleteProduct } = useContext(GlobalContext)
    return (
        <Grid item xs>
            <Card className={classes.root}>
                <CardContent>
                    <Typography className={classes.title} gutterBottom>
                        Product: {product.name}
                    </Typography>
                    <Typography>
                        Price: {product.price}
                    </Typography>
                    <Typography>
                        category: {product.category}
                    </Typography>
                    <Typography>
                        Description: {product.description}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button color='primary' size='small'>Edit</Button>
                    <Button 
                        onClick={() => deleteProduct(product._id)} 
                        size='small' 
                        color='secondary'
                    >
                        Delete
                    </Button>
                </CardActions>
            </Card>
        </Grid>
    )
}

export default ProductItem
