import React, { useContext, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'


import { GlobalContext } from '../context/products'

import ProductItem from './ProductItem'

const ProductList = () => {
    const { products, getProducts } = useContext(GlobalContext)

    useEffect(() => {
        getProducts()
    }, [getProducts])
    
    return (
        <>
          <Grid container spacing={4} justify='center'>
            {products.map(product => (
                <ProductItem key={product._id} product={product} />
            ))}
          </Grid>
        </>
    )
}

export default ProductList

{/* <Grid container spacing={4} justify='center'>
<Grid item xs>
    <Paper style={{ height: 74, width: "100%" }} />
</Grid>
<Grid item xs>
    <Paper style={{ height: 74, width: "100%" }} />
</Grid>
<Grid item xs>
    <Paper style={{ height: 74, width: "100%" }} />
</Grid>
</Grid>   */}