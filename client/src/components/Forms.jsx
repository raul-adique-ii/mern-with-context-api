import React, { useContext } from 'react'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

import { GlobalContext } from '../context/products'

import Form from './Form'
import FormContainer from './FormContainer'
import Input from './Input'
import PrimaryButton from './PrimaryButton'

const schema = yup.object().shape({
    name: yup.string().required('Provide name of the product'),
    price: yup.number().required('Provide price with number not word'),
    category: yup.string().required('Provide category of the product'),
    description: yup.string().required('Provide description of the product'),
    status: yup.string().required('Provide status of the product'),
})

const Forms = () => {
    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: 'onBlur',
        resolver: yupResolver(schema)
    })
    const { addProduct } = useContext(GlobalContext)

    return (
        <FormContainer>
            <Form onSubmit={handleSubmit((data) => addProduct(data))}>
                <Input 
                    {...register('name')}
                    name='name'
                    type='text'
                    label='Name of Product'
                    error={ !!errors.name }
                    helperText={errors?.name?.message}                  
                />
                <Input 
                    {...register('price')}
                    name='price'
                    type='number'
                    label='Price'
                    error={!!errors.price}
                    helperText={errors?.price?.message}               
                />
                <Input 
                    {...register('category')}
                    name='category'
                    type='text'
                    label='Category'
                    error={!!errors.category}
                    helperText={errors?.category?.message}                
                />
                <Input 
                    {...register('description')}
                    name='description'
                    type='text'
                    label='Description'
                    error={!!errors.description}
                    helperText={errors?.description?.message}
                />
                 <Input 
                    {...register('status')}
                    name='status'
                    type='text'
                    label='Status'
                    error={!!errors.status}
                    helperText={errors?.status?.message}
                />
                <PrimaryButton>Add</PrimaryButton>
            </Form>
        </FormContainer>
    )
}

export default Forms
