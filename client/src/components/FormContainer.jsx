import React from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#f4f4f4'
    }
}))

const FormContainer = ({ children, ...props }) => {
    const classes = useStyles()
    return (
        <Container 
            className={classes.root} 
            component='main' 
            maxWidth='xs' 
            { ...props }
        >
            { children }
        </Container>
    )
}

export default FormContainer
