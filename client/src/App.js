// import { useDispatch } from 'react-redux'

import Forms from './components/Forms'
import Header from './components/Header'
import ProductList from './components/ProductList'
import './index.css'

// import { loadProducts } from './store/products'

function App() {
  // const dispatch = useDispatch()
  // dispatch(loadProducts())

  return (
    <div>
      <Header />
      <Forms />
      <ProductList />
    </div>
  );
}

export default App;
