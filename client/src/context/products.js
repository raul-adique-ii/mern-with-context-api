import React, { createContext, useReducer } from 'react'
import axios from 'axios'
import AppReducer from './AppReducer'

// Initial state
const initialState = {
    products: [],
    error: null,
    loading: false
}

// Create context
export const GlobalContext = createContext(initialState)

// Provider component
export const GlobalProvider = ({ children }) => {
    const [state, dispatch] = useReducer(AppReducer, initialState)

    // Actions

    const getProducts = async () => {
        try {
            const response = await axios.get('http://localhost:5000/api/products')
            dispatch({
                type: 'GET_PRODUCTS',
                payload: response.data
            })
        } catch (error) {
            dispatch({
                type: 'PRODUCT_ERROR',
                payload: error.response.data.errors
            })
        }
    }

    const deleteProduct = async (id) => {
        try {
            await axios.delete(`http://localhost:5000/api/products/${id}`)
            dispatch({
                type: 'DELETE_PRODUCT',
                payload: id
            })
        } catch (error) {
            dispatch({
                type: 'PRODUCT_ERROR',
                payload: error.message
            })
        }     
    }

    const addProduct = async (product) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        try {
            const response = await axios.post('http://localhost:5000/api/products', product, config)
            dispatch({
                type: 'ADD_PRODUCT',
                payload: response.data
            })
        } catch (error) {
            dispatch({
                type: 'PRODUCT_ERROR',
                payload: error
            })
        }

       
    }

    return (
        <GlobalContext.Provider value={{
            products: state.products,
            error: state.error,
            loading: state.loading,
            deleteProduct,
            addProduct,
            getProducts
        }}>
            { children }
        </GlobalContext.Provider>
    )
}